/* Varun Radhakrishnan (vradhak2)
   Cindy Yang(xyang73)
   600.120(02)
   Assignment #: final
   Date: 12/7/2016 */
#ifndef _CS120_NGRAM_COLLECTION_HPP
#define _CS120_NGRAM_COLLECTION_HPP
#include <vector>
#include <list>
#include <map>
#include <string>

class NgramCollection {

public:

  //Generate an NgramCollection object with N equal to argument num
  //Also seeds the RNG (there are better ways of ensuring this only happens once, but this will probably be good enough)
  //Note that you may want to change this to a fixed seed
  //for testing purposes.
  NgramCollection(unsigned num) : n(num) { srand(time(NULL)); }

  //Increase count for NgramCollection representing values from begin up to end
  //begin is an iterator to the first word in the Ngram,
  //end is an iterator to the end of the Ngram
  // (so (end - begin) == N)
  void increment(std::vector<std::string>::const_iterator begin,
		 std::vector<std::string>::const_iterator end);

  //Retrieve the value for N
  unsigned getN() const { return n; }

  std::string toString();

  /*int find(std::vector<std::string> key);

  void sort();

  static int compareTo(std::vector<std::string> first, std::vector<std::string> last);*/
  
  std::map<std::vector<std::string>, int>& getMap() {
    return words;
  }
  
private:

  //the collection of entries in this NgramCollection
  std::map<std::vector<std::string>, int> words;

  //the number of items in our NgramCollection
  unsigned n;  
};

#endif
