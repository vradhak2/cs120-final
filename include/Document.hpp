/* Varun Radhakrishnan vradhak2
   Cindy Yang(xyang73)
   600.120(02)
   Assignment #: final
   Date: 12/7/2016 */

#ifndef _CS120_DOCUMENT__HPP
#define _CS120_DOCUMENT__HPP
#include <vector>
#include <list>
#include <unordered_map>
#include <string>
#include "NgramCollection.hpp"
class Document {

public:

  //Generate a LanguageModel with NgramCollection object with N equal to argument n 
  Document(unsigned n, std::string file) : collection(n) {
    name = file;
  }

  //process a file by sending a vector into the increment method of the NgramCollection
  int processFile();

  //Call processFile for each file
  std::string getName() const;

  NgramCollection & getNgram() {
    return collection;
  }

  int compare(Document other);

private:
  NgramCollection collection;
  std::string name;
};

#endif
