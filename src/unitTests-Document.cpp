/* Varun Radhakrishnan (vradhak2)
   600.120 (02)
   Assignment #: 8
   Date: 11/13/2016
 */

#include "../include/catch.hpp" // simple unit-testing framework
#include "../include/Document.hpp" // header declaring the functions we want to test

#include <iostream>
#include <string>
#include <vector>
#include <list>

using std::string;
using std::vector;
using std::list;

//checks if processFile handles invalid inputs
TEST_CASE("Invalid processFile", "[processFile]") {
  Document l3(3,"../data/test1.txt");
  Document l4(3,"notexist");
  REQUIRE(l3.processFile() == 0);
  REQUIRE(l4.processFile() == 1);

}

TEST_CASE("processFile", "[processFile]"){
  Document l2(2,"../data/test2.txt");
  l2.processFile();
  NgramCollection a = l2.getNgram();
  REQUIRE(a.toString() == "an apple 1\nis an 1\nthis is 1\n");
}

TEST_CASE("getname", "[getname]"){
  Document l1(2, "../data/test2.txt");
  REQUIRE(l1.getName() == "../data/test2.txt");
}

TEST_CASE("compare", "[compare]"){
  Document llow(15, "../data/tlow.txt");
  llow.processFile();  
  REQUIRE(llow.compare(llow) == 1);

  
  Document lmedium(8, "../data/tmedium.txt");
  lmedium.processFile();
  REQUIRE(lmedium.compare(lmedium) == 1);

  Document lhigh(5, "../data/thigh.txt");
  lhigh.processFile();
  REQUIRE(lhigh.compare(lhigh) == 1);

  REQUIRE(lhigh.compare(llow) == 0);
}
