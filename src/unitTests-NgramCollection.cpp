/*
  Varun Radhakrishnan (vradhak2)
  600.120 (02)
  Assignment #: 8
  Date: 11/13/2016
 * Unit test cases for NgramCollection class.
 *
 * TODO: some test cases have been provided, but you will need
 * to write your own for the remaining functions
 *
 * While you should definitely add lots more code, try not to remove
 * or modify any of the test cases we've provided
 */

#include "../include/catch.hpp" // simple unit-testing framework
#include "../include/NgramCollection.hpp" // header declaring the functions we want to test

#include <iostream>
#include <string>
#include <vector>
#include <list>

using std::string;
using std::vector;
using std::list;

TEST_CASE("increment", "[increment]"){
  NgramCollection g1(2);
  vector<string> v;
  v.push_back("this");
  v.push_back("is");
  v.push_back("apple");

  g1.increment(v.begin(), v.end()-1);
  REQUIRE(g1.toString() == "this is 1\n");

  g1.increment(v.begin()+1, v.end());
  REQUIRE(g1.toString() == "is apple 1\nthis is 1\n");

}


