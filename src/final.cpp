/* Varun Radhakrishnan (vradhak2)\
   Cindy Yang (xyang73)
   600.120 (02)
   Assignment #: final-project
   Date: 12/7/2016 */

#include "Document.hpp"
#include <iostream>
#include <cstring>
#include<cstdlib>
#include <fstream>
#include <cstring>
#include <exception>

using std::exception;

//compare each file with each other files
//by calling the compare method on each document
int compareFiles(std::vector<Document> documents) {

  for(unsigned i = 0; i < documents.size(); i++) {
    for (unsigned j = i + 1; j < documents.size(); j++) {
      documents.at(i).compare(documents.at(j));
    }
  }

  return 0;
}


//
void getFiles(int n, std::string name, std::vector<Document> &documents) {
  //check if the path of the file is valid 
  std::ifstream List(name);
  if (!List.is_open()) {
    throw exception();
  }

  //read in each file name in the file name list 
  std::string file;
  List >> file;
  while (!List.eof()) {
    Document d(n, file); //construct a document object for each file   
    d.processFile();    // process each file into an NgramCollection
    documents.push_back(d);   //add the obj into the vector of Document objects
    List >> file;
  }

  List.close();    //close the file
}



int main(int argc, char** argv) {
  
  std::vector<Document> documents;

  //check if the number of argument is valid
  if (argc > 3) {
    std::cout << "Invalid Parameters\n";
    return 1;
  }

  //if there's no second argument automatically use medium setting
  //which is 10gram 
  if (argc == 2) {
    try {
      getFiles(10, argv[1], documents);   //read in all the files 
    } catch (exception e) {
      std::cout << "File Reading Error\n";
      return 1;
    }
    compareFiles(documents);    // compare see if there's any plagarism
    return 0;
  }

  //make sure input is valid 
  if (strlen(argv[2]) != 1) {
    std::cout << "Invalid Parameter\n";
    return 1;
  }

  int n = 5;
  switch (argv[2][0]) {
  case 'h' : n = 5;     //high 
       break;
  case 'm' : n = 10;   //medium 
       break;
  case 'l' : n = 20;  //low 
       break;
     default : {
       std::cout << "Invalid Input\n";  
       return 1;
    }
  }


  // read in files and compare to see if there's plagarism 
  try {   
    getFiles(n, argv[1], documents);
  } catch (exception e) {
     std::cout << "File Reading Error\n";
     return 1;
  }
  compareFiles(documents);
  return 0;
}
