/* Varun Radhakrishnan (vradhak2)
   Cindy Yang(xyang73)
   600.120 (02)
   Assignment #: final
   Date: 12/7/2016 */

#include "../include/NgramCollection.hpp"
#include <sstream>
#include <iostream>
#include <algorithm>

//Increase count for NgramCollection representing values from begin up to end
  //begin is an iterator to the first word in the Ngram,
  //end is an iterator to the end of the Ngram
  // (so (end - begin) == N)
void NgramCollection::increment(std::vector<std::string>::const_iterator begin,
				 std::vector<std::string>::const_iterator end) {

  std::vector<std::string> key; //vector that represents key for map


  for (auto iter = begin; iter !=end; iter++) {
    key.push_back(*iter);  //adds to key vector
  }
  words[key] += 1;
}

//converts the Ngramcollection into a string for unit test purpose 
std::string NgramCollection::toString(){
  std::stringstream ss;
  //iterating through the map and print each item 
  for(auto it = words.begin(); it != words.end(); it++){
    for(std::string it1 : it->first){
      ss << it1 << " ";
    }
    ss << it->second << "\n";
}
  return ss.str();
}


