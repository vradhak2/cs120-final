/* Varun Radhakrishnan (vradhak2)
   Cindy Yang(xyang73)
   600.120 (02)
   Assignment #: final
   Date: 12/7/2016 */


#include "../include/Document.hpp"
#include <sstream>
#include <fstream>
#include <iostream>

//processes file into NgramCollection
int Document::processFile() {

  std::vector<std::string> words; //vector of Ngram

  std::ifstream infile(name); //opens file

  if (!infile.is_open()) { //checks if the file openned sucessfully
    infile.close();
    return 1;
  }
   
  for (unsigned int x = 0; x < collection.getN(); x++) { 
    std::string word;
    infile >> word;
    if (infile.eof()) {
      return 1;
    }
    words.push_back(word); //adds n words
  }

  
 
  std::string lastWord;
  while (!infile.eof()) { //loops  until end of file
    
    collection.increment(words.begin(), words.end()); //increments the NgramCollection
    for (unsigned int i = 0; i<collection.getN() - 1; i++) { //moves all elements up one position
      words.at(i) = words.at(i + 1);
    }
    infile >> lastWord;
    if (!infile.eof()) {
      words.at(collection.getN() - 1) = lastWord; //adds last element to the vector
    }

    

  }
  
  infile.close();
  return 0;
  
}

std::string Document::getName() const {
  return name;
}

int Document::compare(Document other) {

  //pass in the address ngram of another document 
  NgramCollection& otherCollection = other.getNgram();

  //get the map of ngram of the document that is called on 
  std::map<std::vector<std::string>, int>& map = collection.getMap();

  //get the map of another document 
  std::map<std::vector<std::string>, int>& otherMap = otherCollection.getMap();

  //iterater through other map
  for (auto iter = otherMap.begin(); iter != otherMap.end(); iter++) {
    //catch plagerism, if map.find returns map.end() if it doesn't find any match element   
    if (map.find(iter->first) != map.end()) {
      std::cout << "Found Plagerisim " << name << " " << other.getName() << "\n";
      return 1;
    }
  }
  
  return 0;
}
